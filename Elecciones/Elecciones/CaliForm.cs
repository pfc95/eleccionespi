﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GMap.NET.WindowsForms;
using GMap.NET.MapProviders;
using GMap.NET.WindowsForms.Markers;

//Paola Fuentes Caro, Sept.2017
namespace Elecciones
{
    public partial class CaliForm : Form
    {
        public CaliForm()
        {
            InitializeComponent();
        }

        private void gmap_Load(object sender, EventArgs e)
        {
            gmap.MapProvider = GMap.NET.MapProviders.GoogleMapProvider.Instance;
            GMap.NET.GMaps.Instance.Mode = GMap.NET.AccessMode.ServerOnly;
            gmap.SetPositionByKeywords("Cali, Colombia");

            GMapOverlay markersOverlay = new GMapOverlay("MarkersOverlay");
            //colegio santa fe
            GMapMarker santafeMarker = new GMarkerGoogle(new GMap.NET.PointLatLng(3.4413114, -76.5100095), GMarkerGoogleType.green);
            markersOverlay.Markers.Add(santafeMarker);
            gmap.Overlays.Add(markersOverlay);
            santafeMarker.ToolTipText = "109 Colegio Santa Fe\nCédulas:9.269";
            //la esperanza
            GMapMarker esperanzaMarker = new GMarkerGoogle(new GMap.NET.PointLatLng(3.3761963, -76.5724283), GMarkerGoogleType.green);
            markersOverlay.Markers.Add(esperanzaMarker);
            gmap.Overlays.Add(markersOverlay);
            esperanzaMarker.ToolTipText = "150 Escuela La Esperanza\nCédulas:17.638";
            //la paz 
            GMapMarker pazMarker = new GMarkerGoogle(new GMap.NET.PointLatLng(3.4495326, -76.5225658), GMarkerGoogleType.green);
            markersOverlay.Markers.Add(pazMarker);
            gmap.Overlays.Add(markersOverlay);
            pazMarker.ToolTipText = "100 La Paz\nCédulas:11.949";
            pazMarker.ToolTipMode = MarkerTooltipMode.Always;//siempre muestra su nombre
            //normal superior los farallone
            GMapMarker normalscMarker = new GMarkerGoogle(new GMap.NET.PointLatLng(3.4500096, -76.5425892), GMarkerGoogleType.green);
            markersOverlay.Markers.Add(normalscMarker);
            gmap.Overlays.Add(markersOverlay);
            normalscMarker.ToolTipText = "120 Normal Superior Los Farallone\nCédulas:11.607";
            //colegio bennett
            GMapMarker benettMarker = new GMarkerGoogle(new GMap.NET.PointLatLng(3.359611, -76.531515), GMarkerGoogleType.green);
            markersOverlay.Markers.Add(benettMarker);
            gmap.Overlays.Add(markersOverlay);
            benettMarker.ToolTipText = "102 Colegio Benett\nCédulas:13.057";
            //colegio los andes 
            GMapMarker andesMarker = new GMarkerGoogle(new GMap.NET.PointLatLng(3.3937665, -76.5400821), GMarkerGoogleType.green);
            markersOverlay.Markers.Add(andesMarker);
            gmap.Overlays.Add(markersOverlay);
            andesMarker.ToolTipText = "105 Colegio Los Andes\nCédulas:12.313";
            //colegio politecnico 
            GMapMarker politecnicoMarker = new GMarkerGoogle(new GMap.NET.PointLatLng(3.4035609, -76.5527008), GMarkerGoogleType.green);
            markersOverlay.Markers.Add(politecnicoMarker);
            gmap.Overlays.Add(markersOverlay);
            politecnicoMarker.ToolTipText = "118 Colegio Politécnico\nCédulas:11.310";
            //univalle sede san fernando
            GMapMarker univalleMarker = new GMarkerGoogle(new GMap.NET.PointLatLng(3.4781416, -76.493185), GMarkerGoogleType.green);
            markersOverlay.Markers.Add(univalleMarker);
            gmap.Overlays.Add(markersOverlay);
            univalleMarker.ToolTipText = "111 Univalle Sede San Fernando\nCédulas:17.511";
            //colegio hispanoamericano 
            GMapMarker hispanoMarker = new GMarkerGoogle(new GMap.NET.PointLatLng(3.4306035, -76.5488345), GMarkerGoogleType.green);
            markersOverlay.Markers.Add(hispanoMarker);
            gmap.Overlays.Add(markersOverlay);
            hispanoMarker.ToolTipText = "107 Colegio Hispanoamericano\nCédulas:11.856";
            //escuela rodrigo lloreda 
            GMapMarker lloredaMarker = new GMarkerGoogle(new GMap.NET.PointLatLng(3.4753969, -76.5073787), GMarkerGoogleType.green);
            markersOverlay.Markers.Add(lloredaMarker);
            gmap.Overlays.Add(markersOverlay);
            lloredaMarker.ToolTipText = "122 Escuela Mario Lloreda\nCédulas:14.639";
            //colegio ciudad cordoba
            GMapMarker cordobaMarker = new GMarkerGoogle(new GMap.NET.PointLatLng(3.4026266,-76.5075513), GMarkerGoogleType.green);
            markersOverlay.Markers.Add(cordobaMarker);
            gmap.Overlays.Add(markersOverlay);
            cordobaMarker.ToolTipText = "130 Colegio Ciudad Cordoba\nCédulas:8.457";
            //universidad antonio josé camacho
            GMapMarker camachoMarker = new GMarkerGoogle(new GMap.NET.PointLatLng(3.401556, -76.5960013), GMarkerGoogleType.green);
            markersOverlay.Markers.Add(camachoMarker);
            gmap.Overlays.Add(markersOverlay);
            camachoMarker.ToolTipText = "104 Universidad Antonio José Camacho\nCédulas:11.161";
            //coliseo el pueblo 
            GMapMarker coliseoMarker = new GMarkerGoogle(new GMap.NET.PointLatLng(3.4156878, -76.5554136), GMarkerGoogleType.green);
            markersOverlay.Markers.Add(coliseoMarker);
            gmap.Overlays.Add(markersOverlay);
            coliseoMarker.ToolTipText = "127 Coliseo El Pueblo\nCédulas:114.469";
            //colegio fundación compartir
            GMapMarker compartirMarker = new GMarkerGoogle(new GMap.NET.PointLatLng(3.456147, -76.5650601), GMarkerGoogleType.green);
            markersOverlay.Markers.Add(compartirMarker);
            gmap.Overlays.Add(markersOverlay);
            compartirMarker.ToolTipText = "128 Colegio Fundación Compartir\nCédulas:5430";
            //escuela nuestra sra de los remedios 
            GMapMarker leongMarker = new GMarkerGoogle(new GMap.NET.PointLatLng(3.4312099, -76.4719022), GMarkerGoogleType.green);
            markersOverlay.Markers.Add(leongMarker);
            gmap.Overlays.Add(markersOverlay);
            leongMarker.ToolTipText = "113 Colegio León de Greiff\nCédulas:8.154";
            //univalle sede melendez
            GMapMarker uniMarker = new GMarkerGoogle(new GMap.NET.PointLatLng(3.3759439, -76.5355843), GMarkerGoogleType.green);
            markersOverlay.Markers.Add(uniMarker);
            gmap.Overlays.Add(markersOverlay);
            uniMarker.ToolTipText = "112 Univalle Sede Melendez\nCédulas:10.436";

        }
        private void uniMarker_OnMarkerClick(GMapMarker item, MouseEventArgs e)
        {
            ResultadosForm resultadoform = new ResultadosForm();
            resultadoform.Show();
            Console.WriteLine(String.Format("Marker {0} was clicked.", item.Tag));
            
        }
        private void uniMarker_OnMarkerClick()
        {
            ResultadosForm resultadoform = new ResultadosForm();
            resultadoform.Show();
        }
    }
}
