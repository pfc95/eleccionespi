﻿namespace Elecciones
{
    partial class ResultadosForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.candidatolabel = new System.Windows.Forms.Label();
            this.votoslabel = new System.Windows.Forms.Label();
            this.candidatoTextB = new System.Windows.Forms.TextBox();
            this.votosTextB = new System.Windows.Forms.TextBox();
            this.fotoCandidato = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.fotoCandidato)).BeginInit();
            this.SuspendLayout();
            // 
            // candidatolabel
            // 
            this.candidatolabel.AutoSize = true;
            this.candidatolabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.candidatolabel.Location = new System.Drawing.Point(24, 25);
            this.candidatolabel.Name = "candidatolabel";
            this.candidatolabel.Size = new System.Drawing.Size(96, 20);
            this.candidatolabel.TabIndex = 0;
            this.candidatolabel.Text = "Candidato:";
            this.candidatolabel.Click += new System.EventHandler(this.label1_Click);
            // 
            // votoslabel
            // 
            this.votoslabel.AutoSize = true;
            this.votoslabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.votoslabel.Location = new System.Drawing.Point(24, 64);
            this.votoslabel.Name = "votoslabel";
            this.votoslabel.Size = new System.Drawing.Size(149, 20);
            this.votoslabel.TabIndex = 1;
            this.votoslabel.Text = "Número de votos:";
            // 
            // candidatoTextB
            // 
            this.candidatoTextB.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.candidatoTextB.Location = new System.Drawing.Point(126, 25);
            this.candidatoTextB.Name = "candidatoTextB";
            this.candidatoTextB.Size = new System.Drawing.Size(288, 26);
            this.candidatoTextB.TabIndex = 2;
            // 
            // votosTextB
            // 
            this.votosTextB.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.votosTextB.Location = new System.Drawing.Point(179, 64);
            this.votosTextB.Name = "votosTextB";
            this.votosTextB.Size = new System.Drawing.Size(125, 26);
            this.votosTextB.TabIndex = 3;
            // 
            // fotoCandidato
            // 
            this.fotoCandidato.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fotoCandidato.Location = new System.Drawing.Point(136, 141);
            this.fotoCandidato.Name = "fotoCandidato";
            this.fotoCandidato.Size = new System.Drawing.Size(154, 196);
            this.fotoCandidato.TabIndex = 4;
            this.fotoCandidato.TabStop = false;
            // 
            // ResultadosForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(432, 388);
            this.Controls.Add(this.fotoCandidato);
            this.Controls.Add(this.votosTextB);
            this.Controls.Add(this.candidatoTextB);
            this.Controls.Add(this.votoslabel);
            this.Controls.Add(this.candidatolabel);
            this.MaximizeBox = false;
            this.Name = "ResultadosForm";
            this.Text = "Resultados";
            this.Load += new System.EventHandler(this.ResultadosForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.fotoCandidato)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label candidatolabel;
        private System.Windows.Forms.Label votoslabel;
        private System.Windows.Forms.TextBox candidatoTextB;
        private System.Windows.Forms.TextBox votosTextB;
        private System.Windows.Forms.PictureBox fotoCandidato;
    }
}